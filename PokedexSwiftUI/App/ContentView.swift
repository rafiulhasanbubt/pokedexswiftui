//
//  ContentView.swift
//  PokedexSwiftUI
//
//  Created by rafiul hasan on 2/10/21.
//

import SwiftUI

struct ContentView: View {
    
    var body: some View {
        PokemonListView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
