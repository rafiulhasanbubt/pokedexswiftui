//
//  PokedexSwiftUIApp.swift
//  PokedexSwiftUI
//
//  Created by rafiul hasan on 2/10/21.
//

import SwiftUI

@main
struct PokedexSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
