//
//  StateView.swift
//  PokedexSwiftUI
//
//  Created by rafiul hasan on 2/10/21.
//

import SwiftUI

struct StateView: View {
    var pokemon: PokemonModel
    var stateName: String
    var stateColor: Color
    var stateValue: Int
    
    var body: some View {
        HStack {
            Text(stateName)
                .font(.system(.body, design: .monospaced))
            
            ZStack(alignment: .leading) {
                RoundedRectangle(cornerRadius: 5)
                    .foregroundColor(.gray)
                    .frame(width: 150, height: 20)
                
                RoundedRectangle(cornerRadius: 5)
                    .foregroundColor(stateColor)
                    .frame(width: stateValue <= 100 ? 150 * (CGFloat(stateValue)/100):150, height: 20)
            }
            
            Text("\(stateValue)")
                .font(.system(.body, design: .monospaced))
        }
    }
}

//struct StateView_Previews: PreviewProvider {
//    static var previews: some View {
//        StateView()
//    }
//}
