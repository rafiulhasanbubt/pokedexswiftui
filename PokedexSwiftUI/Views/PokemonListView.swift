//
//  PokemonListView.swift
//  PokedexSwiftUI
//
//  Created by rafiul hasan on 2/10/21.
//

import SwiftUI

struct PokemonListView: View {
    @ObservedObject var pokemonVM =  PokemonViewModel()
    @State private var searchText = ""
    
    var filterPokemon: [PokemonModel] {
        if searchText == "" { return pokemonVM.pokemon }
        return pokemonVM.pokemon.filter { $0.name.lowercased().contains(searchText.lowercased()) }
    }
    
    var body: some View {
        NavigationView {
            List {
                ForEach(filterPokemon) { poke in
                    NavigationLink(destination: PokemonDetailView(pokemon: poke)) {
                        HStack {
                            VStack(alignment: .leading, spacing: 5) {
                                HStack {
                                    Text(poke.name.capitalized)
                                        .font(.title)
                                    
                                    if poke.isFavorite {
                                        Image(systemName: "star.fill")
                                            .foregroundColor(.yellow)
                                    }
                                }
                                
                                HStack {
                                    Text(poke.type)
                                        .italic()
                                    Circle()
                                        .foregroundColor(poke.typeColor)
                                        .frame(width: 10, height: 10)
                                }
                                
                                Text(poke.description)
                                    .font(.caption)
                                    .lineLimit(2)
                            }
                            
                            Spacer()
                            
                            AsyncImage(url: URL(string: poke.imageURL)) { phase in
                                switch phase {
                                case .empty:
                                    ProgressView()
                                case .success(let image):
                                    image.resizable()
                                        .interpolation(.none)
                                        .scaledToFit()
                                        .frame(width: 100, height: 100)
                                case .failure:
                                    Image(systemName: "photo")
                                @unknown default:
                                    EmptyView()
                                }
                            }
                        }
                    }
                    .swipeActions(edge: .trailing, allowsFullSwipe: false) {
                        Button(action: {
                            addFavorite(pokemon: poke)
                        }) {
                            Image(systemName: "star")
                        }
                        .tint(.yellow)
                    }                    
                }
            }
            .navigationTitle("Pokemon")
            .searchable(text: $searchText)
        }
    }
    
    func addFavorite(pokemon: PokemonModel) {
        if let index = pokemonVM.pokemon.firstIndex(where: {$0.id == pokemon.id}) {
            pokemonVM.pokemon[index].isFavorite.toggle()
        }
    }
}

//struct PokemonListView_Previews: PreviewProvider {
//    static var previews: some View {
//        PokemonListView()
//    }
//}
