//
//  PokemonImageView.swift
//  PokedexSwiftUI
//
//  Created by rafiul hasan on 2/10/21.
//

import SwiftUI

struct PokemonImageView: View {
    var imageUrl: String
    
    var body: some View {
        
        AsyncImage(url: URL(string: imageUrl)) { phase in
            switch phase {
            case .empty:
                ProgressView()
            case .success(let image):
                image.resizable()
                    .interpolation(.none)
                    .scaledToFit()
                    .frame(width: 200, height: 200)
            case .failure:
                Image(systemName: "photo")
            @unknown default:
                EmptyView()
            }
        }
        /*
        image.resizable()
            .scaledToFill()
            .frame(width: 200, height: 200)
            .clipShape(Circle())
            .overlay(Circle().stroke(Color.white, lineWidth: 2))
            .background(Circle().foregroundColor(.white))
            .shadow(radius: 5)
         */
    }
}

//struct PokemonImageView_Previews: PreviewProvider {
//    static var previews: some View {
//        PokemonImageView(imageUrl: .constant("url"))
//    }
//}
