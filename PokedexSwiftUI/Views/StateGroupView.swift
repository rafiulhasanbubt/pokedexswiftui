//
//  StateGroupView.swift
//  PokedexSwiftUI
//
//  Created by rafiul hasan on 2/10/21.
//

import SwiftUI

struct StateGroupView: View {
    var pokemon: PokemonModel
    
    var body: some View {
        ZStack {
            Rectangle()
                .frame(width: 300, height: 250)
                .foregroundColor(.white)
                .opacity(0.4)
                .cornerRadius(20)
            
            VStack(alignment: .leading, spacing: 30) {
                StateView(pokemon: pokemon, stateName: "Atk", stateColor: .blue, stateValue: pokemon.attack)
                StateView(pokemon: pokemon, stateName: "Def", stateColor: .red, stateValue: pokemon.defense)
                StateView(pokemon: pokemon, stateName: "Hgt", stateColor: .teal, stateValue: pokemon.height)
                StateView(pokemon: pokemon, stateName: "Wgt", stateColor: .cyan, stateValue: pokemon.weight)
            }
            
        }
    }
}

//struct StateGroupView_Previews: PreviewProvider {
//    static var previews: some View {
//        StateGroupView()
//    }
//}
