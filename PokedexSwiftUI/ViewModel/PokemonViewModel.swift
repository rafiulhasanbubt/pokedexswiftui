//
//  PokemonViewModel.swift
//  PokedexSwiftUI
//
//  Created by rafiul hasan on 2/10/21.
//

import Foundation
import SwiftUI

enum FetchError: Error {
    case badUrl
    case badResponse
    case badData
}

@MainActor
class PokemonViewModel: ObservableObject {
    @Published var pokemon = [PokemonModel]()
    
    init() {
        Task {
            pokemon = try await getPokemon()            
        }
    }
    
    func getPokemon() async throws -> [PokemonModel] {
        guard let url = URL(string: "https://pokedex-bb36f.firebaseio.com/pokemon.json") else { throw FetchError.badUrl}
        let urlRequest = URLRequest(url: url)
        let (data, response) = try await URLSession.shared.data(for: urlRequest)
        
        guard (response as? HTTPURLResponse)?.statusCode == 200 else { throw FetchError.badResponse }
        guard let data = data.removeNullsFrom(string: "null,") else { throw FetchError.badData}
        let pokemonData = try JSONDecoder().decode([PokemonModel].self, from: data)
        return pokemonData
        
    }
}

extension Data {
    func removeNullsFrom(string: String) -> Data? {
        let dataString = String(data: self, encoding: .utf8)
        let parseDataString = dataString?.replacingOccurrences(of: string, with: "")
        guard let data = parseDataString?.data(using: .utf8) else {return nil}
        return data
    }
}
